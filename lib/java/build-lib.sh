#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

_mvn() {
  mvn \
    --batch-mode \
    --errors \
    --fail-at-end \
    --show-version \
    -DdeployAtEnd=true \
    -Dhttps.protocols=TLSv1.2 \
    -DinstallAtEnd=true \
    -Djava.awt.headless=true \
    -Dmaven.repo.local="${CI_PROJECT_DIR}/.m2/repository" \
    -Dorg.slf4j.simpleLogger.showDateTime=true \
    "$@"
}

_mvn clean verify

if [ "${CI_COMMIT_BRANCH:-}" == "main" ]; then
  _mvn deploy -s "${ENUAGES_CI_HOME}/lib/java/mvn-ci-settings.xml"
fi
